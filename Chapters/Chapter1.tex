
\chapter{Introducción General} % Main chapter title
\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}
\newcommand{\grados}{$^{\circ}$}

%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------

\section{El procesador LEON3}

El procesador LEON3 es uno de los miembros de la familia de procesadores LEON desarrollada por la ESA (European Space Agency) para servir de plataforma en todas sus misiones espaciales. Es un procesador de arquitectura tipo SPARC v8, configurable, y disponible en forma de código fuente bajo licencia GPL (versión 2.0) en el lenguaje de descripción de hardware VHDL. 

El origen de este procesador se remonta al año 1992, cuando la agencia espacial europea dio inicio a un proyecto para desarrollar un diseño de procesador libre de todo tipo de compromisos de licenciamiento con empresas privadas, y cuya disponibilidad no estuviera atada a los ciclos de vida de los productos comerciales.

Luego de un proceso de selección donde se analizaron las ventajas y desventajas de una serie de arquitecturas (SPARC, MIPS, NS32, M88K, AMD29K) se seleccionó la arquitectura SPARC v8, la cual tiene la virtud de estar definida como un estándar IEEE \cite{ref.SparcIeeeStandard} por lo que los diseños basados en ella no tiene ningún tipo de restricciones de uso ni costo de licenciamiento. 

A partir de este estándar se realizó el desarrollo del procesador que sería bautizado como LEON1, el cual fue implementado en el lenguaje de descripción de hardware VHDL y publicado bajo una licencia de código abierto en el año 2000 \cite{ref.ESASpaceAgencyLaunchesFreeSparcLikeCore}\cite{ref.leonSparcProcessorPastPresentFuture}. A este diseño le siguieron posteriormente los procesadores LEON2, LEON3 y ahora LEON4, que presentan mejoras en los sistemas auxiliares, mayor capacidad de cómputo, y la habilidad de implementar sistemas multi-núcleo mientras mantienen la misma arquitectura base y la compatibilidad del set de instrucciones.

El diseño en lenguaje VHDL de estos procesadores les confiere una enorme portabilidad, pudiendo realizarse implementaciones tanto en versiones ASIC como en lógica programable FPGA. Para su uso en misiones espaciales existen versiones FT ({Fault-Tolerant}) que incorporan medidas de enmascaramiento y detección de fallas inducidas por radiación, e implementadas sobre circuitos diseñados con tecnologías {Rad-Hard} capaces de soportar el deterioro provocado por la radiación en el espacio exterior.

En la actualidad el desarrollo de la familia de procesadores LEON se encuentra en manos de la firma {Cobham Gaisler AB}. Esta última se encarga del mantenimiento y desarrollo de los procesadores LEON3 y LEON4; LEON3 mantiene un esquema de licenciamiento idéntico al de las anteriores versiones provistas por la ESA: libre disponibilidad de una versión común bajo la licencia GPL, y la comercialización de una versión FT del mismo diseño para aplicaciones de tipo aeroespacial. En el caso de LEON4 no existe ninguna versión de código abierto del diseño, y sólo puede accederse a su utilización mediante la obtención de una licencia comercial de {Cobham Gaisler AB}. 

Además de los diseños en código fuente, la firma {Cobham Gaisler AB} también comercializa implementaciones Rad-Hard en ASIC de las versiones FT de los procesadores LEON3 y LEON4, y aporta al crecimiento del ecosistema de desarrollo de sistemas basados en estos procesadores participando del desarrollo de compiladores y herramientas de depuración, facilitando documentación de interfaces y portando sistemas operativos para que corran en estas plataformas (RTEMS, eCos, Linux, FreeRTOS, VxWorks, Nucleus y ThreadX).

A partir de su tercera versión los procesadores LEON no son componentes independientes sino que se encuentran insertos dentro de una biblioteca de componentes sintetizables ({IP cores}) descriptos en VHDL llamada GRLIB.
Esta última incluye todo tipo de elementos necesarios para conformar un {System-on-Chip} completo alrededor de los procesadores LEON, como por ejemplo:
\begin{itemize}
\item Controladores de memoria (convencional y FT).
\item Controlador de interrupciones.
\item Unidades de punto flotante (FPU).
\item Temporizadores, módulos de captura de eventos, watchdogs.
\item Canales de comunicación:\begin{itemize}
\item UARTs,  SPI, I2C, Ethernet, SpaceWire, JTAG, MIL-ST-1553B, CAN, PS/2.
\end{itemize}
\item Controladores de memoria flash.
\item Buses\begin{itemize}
\item AMBA (nativo), PCI, USB.
\end{itemize}
\item Módulos DMA.
\item Generador de texto en video VGA.
\item Generador PWM.
\end{itemize}
Muchos de estos módulos se encuentran disponibles en la GRLIB como código abierto bajo licencia GPL, pero los destinados a aplicaciones más especializadas o aquellos disponibles en versiones tolerantes a falla requieren una licencia comercial otorgada por {Cobham Gaisler AB}. Una lista completa de los módulos disponibles así como las licencias bajo las cuales se encuentran cubiertos se puede encontrar en el {GRLIB IP Core User’s Manual}, referencia \cite{ref.GrlibIpCoreUsersManual}.

Armado de este paquete de herramientas el diseñador tiene la libertad dimensionar el sistema a medida de cada aplicación particular, incorporando dispositivos solamente en la medida exacta de los demandado por los requerimientos. 

%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------


\section{Sistemas basados en lógica programable}
\label{sec:sistemasbasadosenlogicaprogramable}

Los sistemas basados en dispositivos de lógica programable FPGA y CPLD son circuitos que se caracterizan por su capacidad de permitir la reconfiguración de su comportamiento una o múltiples veces de forma posterior a su fabricación, con el fin de reemplazar el diseño implementado, corregir defectos, reafectarlos a tareas diferentes, etc.

Se diferencian claramente de los sistemas basados en circuitos ASIC, los cuales ven todas sus capacidades, virtudes y defectos definitivamente establecidos en el momento mismo de la impresión del diseño sobre la pastilla de silicio, imposibilitando cualquier modificación posterior a su comportamiento más allá de lo que las señales de control disponibles o la programabilidad del software que sean capaces de correr permita.

% ----------

% ventajas

Esta diferencia fundamental presenta una clara ventaja para el diseñador de sistemas, ya que permite aplicar al desarrollo un ciclo de vida más semejante al del desarrollo de software que al de hardware:\begin{itemize}
\item La definición del comportamiento definitivo del dispositivo puede ser postergada más allá de la fecha de fabricación de hardware, reduciendo de esa forma el impacto de esta actividad en el camino crítico del proyecto.
%\item Se abaratan enormemente los costos de desarrollo, ya que sobre una misma versión del hardware se puede iterar múltiples versiones del diseño lógico de la configuración del dispositivo de lógica programable, reduciendo la cantidad de prototipos necesarios y agilizando el desarrollo al eliminar los tiempos muertos que requiere la fabricación de los mismos.
\item Se simplifica la resolución de problemas en el diseño, ya que una vez que estos han sido detectados para corregirlos solamente es necesario realizar una reconfiguración del dispositivo.
\end {itemize}


Los dispositivos de lógica programable pueden ser utilizados no solamente para implementar subsistemas dentro de un sistema mayor (como por ejemplo, un dispositivo altamente especializado que funciona asociado a sistema de procesamiento desarrollado con ASICs), sino que gracias a bibliotecas de {IP cores} como la GRLIB es posible construir sistemas completos dentro de una misma pastilla de circuito integrado ({System-on-Chip}, SoC) que incluyan el microprocesador, los controladores de memoria, los buses de alta velocidad, los puertos de comunicación, cualquier dispositivo especializado requerido por la aplicación, etc. 

Esto es especialmente útil para el desarrollo de prototipos y de productos de baja tirada \cite{ref.surveyfpgavsasic} \cite{ref.fpgavsasicwhattochoose}, ya que reduce sensiblemente los costos fijos de la placa al minimizar la complejidad a nivel de circuito impreso, reducir la cantidad de circuitos integrados y la cuenta total de pines, simplificar el ruteo y facilitar notablemente la gestión de la integridad de señales a nivel de sistema. 


% ----------

% desventajas

Esta flexibilidad tiene costo, ya que los dispositivos de lógica programable utilizados deberán estar dimensionados de forma tal que puedan contener no solamente la suma de los recursos que son realmente necesarios llevar adelante un diseño dado, sino la suma de los recursos que pudieran ser necesarios para acomodar cualquier diseño dentro de una dada gama de complejidad. Lo anterior implica que desde la óptica de cualquier proyecto individual, el dispositivo de lógica programable utilizado siempre está sobredimensionado respecto de los requerimientos reales de la aplicación. 

Los diseños basados en lógica programable también presentan otras desventajas frente a los ASIC en otros índices de desempeño:
\begin{itemize}
\item Altos niveles de complejidad inicial en los diseños basados en lógica programable, independientemente de la complejidad real del proyecto: elevado número de patas por circuito impreso, múltiples fuentes de alimentación, memorias auxiliares para contener la configuración de la lógica, etc.
\item Elevado consumo de potencia, estático y dinámico.
\item Frecuencias de operación máxima relativamente bajas (hasta aproximadamente $600\ \mbox{Mhz}$).
\item Alto costo por dispositivo, y virtual ausencia de economía de escala.
\end{itemize}

De todo lo anterior se desprende que los sistemas basados en lógica programable presentan importantes ventajas en las siguientes áreas del diseño:
\begin{itemize}
\item En el diseño de sistemas sensibles al costo variable (fabricación masiva), como herramienta de prototipado durante las fases de diseño de un circuito ASIC.
\item En el diseño de sistemas sensibles al costo fijo (fabricación en cantidades limitadas).
\item En sistemas poco sensibles al costo, pero que demandan alta flexibilidad: herramientas de laboratorio e instrumentación.
\end{itemize}
y deben ser evitados en las siguientes áreas:
\begin{itemize}
\item Sistemas de baja complejidad que puedan ser implementados mediante componentes discretos preexistentes (compuertas, contadores, multiplexores, decodificadores, etc.).
\item Sistemas de procesamiento de muy alta frecuencia de operación.
\item Sistemas de bajo consumo (móviles).
\end{itemize}

%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------


\section{El Proyecto CIAA}
\label{sec:proyectociaa}

El Proyecto de la Computadora Industrial Abierta Argentina (Proyecto CIAA) es un esfuerzo multilateral conjunto tanto de sectores privados como públicos cuyo objetivo es el de impulsar la modernización tanto de los medios de producción como de los bienes de consumo generados por la industria nacional, al facilitar el acceso de la misma a una plataforma de electrónica abierta que amplia disponibilidad y bajo riesgo de inversión.

El objetivo central de este proyecto es el desarrollo de la Computadora Industrial Abierta Argentina (CIAA), que es una plataforma electrónica programable de diseño abierto y calidad de grado industrial. Ésta se encuentra formada por un sistema de cómputo basado en un microcontrolador de alto rendimiento y un conjunto de interfaces de entrada/salida capaces de interactuar con todo tipo de actuadores y sensores frecuentemente utilizados en la industria (relés, sensores de contacto, luces, etc.).

Relacionado con el desarrollo anterior, surge al amparo del mismo proyecto la CIAA Educativa (EDU-CIAA), la cual es una plataforma de cómputo semejante a la CIAA Industrial pero de menor costo y más adaptada para ser utilizada en contextos académicos y de laboratorio. Este proyecto se sinergiza con el de la CIAA Industrial al reducir el costo de entrenamiento y desarrollo de prototipos, y amplifica el alcance del Proyecto CIAA al servir de punto de entrada al mismo en entidades educativas secundarias técnicas y terciarias.

Con el fin de maximizar el grado de independencia de las plataformas CIAA y EDU-CIAA respecto del ciclo de vida comercial de los microcontroladores utilizados en las mismas es que se realizó de forma simultánea el desarrollo de múltiples versiones basadas en dispositivos de múltiples orígenes. 

De esta forma surgieron las computadoras industriales CIAA-NXP (basada en el microcontrolador LPC4337 de NXP), la CIAA-FSL (basada en el microcontrolador Kinetis K60 de Freescale), y se encuentra en desarrollo la CIAA-PIC (diseñada alrededor del microcontrolador PIC32MZ de la firma Microchip). 

De forma semejante son dos las versiones educativas de la computadora CIAA existentes en la actualidad:  la EDU-CIAA-NXP y la EDU-CIAA-FSL, ambas basadas en los mismos microcontroladores que sus contrapartes industriales.

%Esta última merece una mención especial por su relevancia para el presente trabajo. La Edu-CIAA-Xilinx es una variante de la versión educativa de la Edu-CIAA que tiene la particularidad de no estar basada en un microcontrolador ASIC sino en un dispositivo lógico programable de tipo FPGA Artix-7 de la firma Xilinx. Tal y como se discute en la Sección \ref{sec:sistemasbasadosenlogicaprogramable} acerca de los dispositivos lógicos programables, la cualidad de poder reconfigurar la lógica de control le confiere a la Edu-CIAA-Xilinx una flexibilidad y una capacidad de adaptación que la convierten en una excelente herramienta de prototipado de sistemas.
%
%Dispositivos como la Edu-CIAA-Xilinx tienen potencialmente la capacidad de ajustarse a los requerimientos de cada aplicación particular, permitiendo dimensionar la cantidad de UARTS disponibles, habilitar o deshabilitar la disponibilidad de generadores PWM, o incluso incorporar dispositivos altamente especializados como por ejemplo controladores de paneles LED o variadores de velocidad para motores trifásicos.

Cabe mencionar que se encuentran en vías desarrollo otras dos variantes CIAA destinadas a segmentos de usuarios con necesidades más específicas, llamadas CIAA-ACC (Alta Capacidad de Cómputo, para aplicaciones de procesamiento intensivo de datos), y la CIAA-Safety (para aplicaciones críticas y con requerimientos de tolerancia a fallas). Por formar éstas una vertiente de desarrollo totalmente independiente tanto en hardware como en software de las variantes que son relevantes para este trabajo, nada más se dirá de estas en el resto de este informe.

Para evitar que esta multiplicidad de versiones de hardware fragmente la base de usuarios es que se desarrolló una interfaz de software común a todas la versiones de la CIAA industriales y educativas, de forma de simplificar la portabilidad de las aplicaciones de usuario entre ellas y facilitar la migración de una a otra. Esta interfaz de software es el llamado {Firmware CIAA}.

El Firmware CIAA tiene una estructura modular y estratificada que en su nivel más bajo se encuentra formada por drivers para los dispositivos de hardware y un sistema operativo de tiempo real basado en el estándar de la industria automotriz OSEK-VDX. El usuario puede optar por programar el sistema directamente a través de las interfaces provistas por estos módulos en lenguaje C, o hacerlo a un nivel de abstracción más elevado a través de interfaces que permiten al Firmware CIAA operar como ejecutor de un programa escrito en el lenguaje {Ladder}, el cual es un método estandarizado de programación de Controladores Lógicos Industriales (PLC).


%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------

\section{Objetivos, motivación y alcance}

El objetivo del presente trabajo fue el de aumentar el alcance del Firmware CIAA al incorporar al mismo el código necesario para permitir su ejecución sobre sistemas basados en el procesador LEON3. Esto persiguió un doble propósito.

Por un lado, se buscaba enriquecer el abanico de sistemas operativos disponibles para los desarrolladores de sistemas basados en el procesador LEON3 al incorporar el Firmware CIAA y su sistema operativo FreeOSEK. Este es un aporte que puede considerarse objetivamente valioso:
\begin{itemize}
\item El Firmware CIAA es sostenido por una comunidad, no por una empresa.
\item El sistema es relativamente simple, por lo que presenta una curva de aprendizaje plana.
\item La base de código es pequeña y flexible, de fácil modificación para aplicaciones específicas.
\item Presenta mínimos requisitos de recursos para el sistema, y estos se encuentran perfectamente definidos en tiempo de compilación.
\item Licencia libre tipo BSD, que no inhibe la realización de trabajos derivados cerrados.
\item Las interfaces de FreeOSEK están definidas en el estándar OSEK-OS, permitiendo reemplazarlo por una implementación alternativa del mismo estándar con mínimo impacto en la aplicación del usuario.
\end{itemize}

El segundo objetivo fue el de ampliar la base de plataformas hardware donde es posible desarrollar aplicaciones utilizando las interfaces del Firmware CIAA. Gracias a la capa de compatibilidad desarrollada es posible crear aplicaciones basadas en el Firmware que se ejecuten sobre cualquier plataforma de hardware que sea capaz de integrar un sistema basado en el procesador LEON3. Esto abarca una amplia variedad de sistemas basados en lógica programable, incluyendo muchos kits de desarrollo basados en FPGAs como los utilizados frecuentemente en los laboratorios de sistemas digitales.

Puede señalarse también que el sistema resultante de la fusión entre el Firmware CIAA y un sistema LEON3 es un excelente ejemplo de sistema libre: utiliza un sistema operativo libre basado en un estándar automotriz (OSEK), sobre un sistema basado en un bus de altas prestaciones basado en otro estándar abierto (AMBA) poblado de dispositivos proveniente de una biblioteca de IP Cores disponibles en forma de código abierto y con licencia de uso libre (la GRLIB), en un procesador que es un estándar IEEE (LEON3), y todo esto puede ser sintetizado sobre una variedad de dispositivos FPGA provenientes de la práctica totalidad de los fabricantes en el mercado.

Dado que la ejecución de este trabajo se encuentra enmarcada en el Proyecto Final de la Carrera de Especialización de la Universidad de Buenos Aires, el cual por reglamento debe ser finalizado dentro de un período de diez meses para ser válido, se decidió limitar el alcance del proyecto a un conjunto de metas mínimas tales que a fecha de cierre se contara con los elementos para realizar la puesta en marcha de un sistema funcional que pudiera ser ampliado posteriormente por el autor o por terceros sin necesidad de un conocimiento profundo de la arquitectura del sistema LEON3.

De esta forma se excluyeron de la implementación todas aquellas características del sistema que no fueran esenciales para  su implementación o validación. Ejemplos de elementos excluidos con este criterio fueron la realización de versiones funcionales de los drivers de dispositivos AIO (entradas/salidas analógicas), DIO (entradas/salidas digitales de propósito general), y memorias Flash, y la capacidad de funcionar en sistemas con unidades de punto Flotante (FPU).

A partir del criterio anterior se delimitó el alcance del proyecto al siguiente conjunto de metas clave:
\begin{itemize}
\item Desarrollar el código necesario para portar el núcleo del sistema operativo OSEK para que pueda correr sobre el procesador LEON3. 
\item Desarrollar el los drivers necesarios para habilitar la entrada/salida por UART del firmware de la CIAA.
\item Incorporar la nueva plataforma al sistema de compilación del firmware.
\item Documentar el conjunto de herramientas necesario para compilar y ejecutar el firmware para la plataforma LEON3.
\item Documentar las características mínimas requeridas por el firmware de la plataforma SoC  formada por el procesador y sus dispositivos.
\item Hacer disponible el código y la documentación generados para su evaluación y posible integración a la versión oficial del Firmware CIAA. 
\end{itemize}
a las que posteriormente se agregó una meta adicional:
\begin{itemize}
\item Validar el módulo de sistema operativo OSEK ya portado utilizando la batería de tests estandarizados que se encuentran  disponibles como parte del sistema de verificación del Firmware CIAA.
\end{itemize}



%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------

\section{Antecedentes del proyecto}

El autor se desempeña hace varios años dentro de un grupo que lleva adelante el desarrollo de sistemas de navegación basados en la recepción de señales de los sistemas de posicionamiento global GPS y GLONASS, destinados a ser utilizados en vehículos aeroespaciales. Esto se lleva a cabo en el marco de una serie de convenios realizados entre la Universidad Nacional de La Plata (UNLP) y la Comisión Nacional de Actividades Espaciales (CONAE) \cite{ref.glpugaeamta2010}\cite{ref.glpugasase2014}.

Es en este contexto que el autor trabaja regularmente con sistemas basados en lógica programable que implementan la sección de procesamiento digital de los receptores. Estos sistemas se encuentran formados por sistema de cómputo basado en un procesador LEON3 y un dispositivo especialmente dedicado al procesamiento de señales de sistemas de posicionamiento global (correlador de señales GPS/GLONASS de 24 canales).

El software de este sistema se encuentra formado por múltiples tareas de ejecución concurrente encargadas de ejecutar los múltiples tipos de algoritmos que son necesarios en este tipo de receptores. Para reducir la complejidad del sistema se utiliza un sistema operativo de tiempo real que libera a los programadores de resolver las cuestiones de sincronización entre tareas, la planificación de conjunto y el control de dispositivos a bajo nivel. 
En los proyectos actualmente desarrollados el sistema operativo utilizado es el RTEMS, el cual es un sistema operativo de tiempo real, código abierto y licencia GPL frecuentemente utilizado en el ambiente aeroespacial. 

Basado en la observación de éste y otros proyectos similares es que se llegó a apreciar la ventaja rotunda que significa contar con la asistencia de un sistema operativo en el diseño de software para sistemas embebidos, en claro contraste con la alternativa conocida frecuentemente como \textsl{bare-metal}. 

Se observó que la mayor parte de los sistemas operativos disponibles para sistemas LEON3 se encuentran diseñados pensando en servir de soporte a una gama de aplicaciones de elevada complejidad, con requerimientos sofisticados de administración de recursos y sincronización entre tareas.

La contracara de esta riqueza de capacidades es que su utilización consume amplias cantidades de memoria para alojar las estructuras de datos y programa del sistema operativo, y que el aprendizaje de estos sistemas generalmente presenta una curva de aprendizaje ardua. Por otro lado la realización de modificaciones para adaptar el sistema a una aplicación particular puede resultar problemática o incluso imposible para alguien sin experiencia previa con el sistema operativo y con las internalidades de su código fuente. 

Todo esto resulta disuasivo para los diseñadores de toda una gama de aplicaciones de mediana y baja complejidad que tienen requisitos de gestión modestos, las cuales podrían verse beneficiadas de la estructura que provee un sistema operativo multitarea pero que no pueden justificar la complejidad adicional que su utilización introduce en el diseño y en el proceso de desarrollo. 

Es con la intención de generar una alternativa para este último segmento de usuarios de los sistemas LEON3 que se llegó a contemplar la idea de portar el código de un sistema operativo de tiempo real más pequeño y ágil, de bajo consumo de recursos, orientado a aplicaciones de baja complejidad, y que presente una base de código accesible. En todos estos sentidos, el sistema operativo FreeOSEK incluido en el Firmware CIAA se presentaba como un excelente candidato.

%----------------------------------------------------------------------------------------







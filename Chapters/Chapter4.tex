
\chapter{Ensayos y Resultados} % Main chapter title
\label{Chapter4} 


%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------

En este capítulo se presentan los resultados de los ensayos realizados para validar el código de la capa de compatibilidad desarrollada.

Se comienza describiendo las características de los sistemas utilizados para ejecutar los ensayos. Uno de ellos es un modelo de sistema LEON3 sintetizado en un kit de desarrollo FPGA y el otro un modelo de sistema ejecutado en un simulador de sistemas LEON3 provisto por Cobham Gaisler. 

Las secciones posteriores enumeran los resultados de los ensayos realizados para verificar el funcionamiento del sistema operativo, del driver de la UART, y del código asociado al temporizador del sistema del Firmware CIAA ejecutado en una plataforma LEON3.
	
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------


\section{Modelos de sistema utilizados}

Con el fin de verificar el funcionamiento del código desarrollado se pusieron en marcha dos modelos de sistema LEON3, cada uno con sus propias características. El primero es una implementación de sistema basado en la GRLIB en un kit de desarrollo FPGA, mientras que el segundo es un sistema LEON3 simulado que corre en la versión de evaluación del software TSIM que se puede descargar desde la página de Cobham Gaisler.

Ambos modelos pueden ser controlados mediante le protocolo de depuración remota del programa de depuración GDB por lo que desde el punto de vista de la carga y ejecución de los programas ambos se comportan de manera prácticamente idéntica e intercambiable entre sí. 

Son diferentes las características de los sistemas LEON3 implementados dentro de cada sistema, con diferencias en la frecuencia de sistema, tipo y cantidad de dispositivos disponibles, características de los dispositivos, etc. Estas diferencias, sin embargo, son las que el código desarrollado en este trabajo puede acomodar mediante los mecanismos de autodetección del hardware presente, por lo que no son necesarias modificaciones al código del Firmware CIAA desarrollado para pasar de un modelo a otro.

Esta adaptabilidad del código del Firmware CIAA desarrollado a diferentes implementaciones físicas del sistema LEON3 es extrapolable a cualquier otra plataforma basada en el mismo procesador, con la única condición de que esta última satisfaga los requisitos mínimos enumerados en la Sección \ref{sec.caracteristicasminimas}.

En las subsecciones siguientes se profundizará sobre las características de cada uno de los dos modelos de hardware utilizados.


\subsection{Modelo en kit DE2-115}

\label{sec.modelohardware}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{./Figures/kitde2115.jpg}
	\caption[Kit de desarrollo DE2-115.]{Kit de desarrollo para FPGA DE2-115 utilizado para instanciar uno de los sistemas LEON3 utilizados  durante los ensayos. Fotografía de Terasic.}
	\label{fig.kitde2115}
\end{figure}

El primero de estos sistemas es un sistema sintetizado en hardware en un kit de desarrollo para FPGA modelo DE2-115 de la firma Terasic, cuya imagen puede verse en la Figura \ref{fig.kitde2115}. Este kit de desarrollo se encuentra diseñado alrededor de FPGA Cyclone IV de la firma Altera, y contiene numerosos dispositivos adicionales:
\begin{itemize}
\item Ethernet 10/100/1000M.
\item Puertos PS/2, VGA Out y RS-232.
\item Puertos de audio: mic in/mic out/line out.
\item Puertos USB Host y USB Device.
\item Módulo LCD de 16x2.
\item 6 displays de 7 segmentos. 
\item 18 interruptores deslizantes, 4 pulsadores, y 26 LEDs para interfaces de usuario genéricas.
\item Oscilador interno de $50\ \mbox{MHz}$.
\item Entrada para oscilador externo.
\item Múltiples memorias: 2MB SRAM, 8MB Flash, 64MB SDRAM.
\end{itemize}

Junto con el código fuente de la biblioteca GRLIB vienen diseños que permiten sintetizar sistemas basados en la misma sobre una amplia gama de plataformas basadas en FPGA de diferentes fabricantes y modelos, incluyendo un diseño especialmente preparado para ser instanciado sobre el kit de desarrollo DE2-115 de Altera.

El sistema LEON3 generado de este modo implementa dispositivos que hacen uso de varios de los puertos físicos disponibles en la placa, y viene preparado para acceder a la unidad de depuración del procesador a través del puerto \textit{Byte-Blaster} del kit de desarrollo utilizando el software GRMON. 

GRMON es capaz de obtener una descripción detallada del hardware sintetizado en el sistema LEON3. Esta información es obtenida leyendo las tablas de enumeración de dispositivos descriptas en la Sección \ref{sec.enumeraciondehardware} y complementando dicha información con los detalles de configuración de cada dispositivo individual que se puede hallar distribuidos en los registros de control y estado de estos. 

En el caso del sistema implementado en el kit DE2-115, la utilización del programa GRMON para enumerar las características del sistema arroja la siguiente información:

\begin{verbatim}
glpuga@HOST:~/$ grmon -altjtag 

  GRMON2 LEON debug monitor v2.0.78 32-bit eval version
  
  Copyright (C) 2016 Cobham Gaisler - All rights reserved.
  For latest updates, go to http://www.gaisler.com/
  Comments or bug-reports to support@gaisler.com
  
 JTAG chain (1): EP3C120/EP4CE115 
  GRLIB build version: 4156
  Detected frequency:  50 MHz
  
  Component                            Vendor
  LEON3 SPARC V8 Processor             Cobham Gaisler
  AHB Debug UART                       Cobham Gaisler
  JTAG Debug Link                      Cobham Gaisler
  GR Ethernet MAC                      Cobham Gaisler
  LEON2 Memory Controller              European Space Agency
  AHB/APB Bridge                       Cobham Gaisler
  LEON3 Debug Support Unit             Cobham Gaisler
  Generic UART                         Cobham Gaisler
  Multi-processor Interrupt Ctrl.      Cobham Gaisler
  Modular Timer Unit                   Cobham Gaisler
  General Purpose I/O port             Cobham Gaisler
  SPI Controller                       Cobham Gaisler
  AHB Status Register                  Cobham Gaisler
  
  Use command 'info sys' to print a detailed report of attached cores

grmon2> info sys
  cpu0      Cobham Gaisler  LEON3 SPARC V8 Processor    
            AHB Master 0
  ahbuart0  Cobham Gaisler  AHB Debug UART    
            AHB Master 1
            APB: 80000700 - 80000800
            Baudrate 115200, AHB frequency 50.00 MHz
  ahbjtag0  Cobham Gaisler  JTAG Debug Link    
            AHB Master 2
  greth0    Cobham Gaisler  GR Ethernet MAC    
            AHB Master 3
            APB: 80000E00 - 80000F00
            IRQ: 12
            edcl ip 192.168.0.51, buffer 2 kbyte
  mctrl0    European Space Agency  LEON2 Memory Controller    
            AHB: 00000000 - 20000000
            AHB: 40000000 - 80000000
            APB: 80000000 - 80000100
            8-bit prom @ 0x00000000
            32-bit sdram: 1 * 128 Mbyte @ 0x40000000
            col 10, cas 2, ref 7.8 us
  apbmst0   Cobham Gaisler  AHB/APB Bridge    
            AHB: 80000000 - 80100000
  dsu0      Cobham Gaisler  LEON3 Debug Support Unit    
            AHB: 90000000 - A0000000
            AHB trace: 128 lines, 32-bit bus
            CPU0:  win 8, hwbp 2, itrace 128, V8 mul/div, srmmu, lddel 1
                   stack pointer 0x47fffff0
                   icache 4 * 4 kB, 32 B/line, lru
                   dcache 4 * 4 kB, 16 B/line, lru, snoop tags
  uart0     Cobham Gaisler  Generic UART    
            APB: 80000100 - 80000200
            IRQ: 2
            Baudrate 38343, FIFO debug mode
  irqmp0    Cobham Gaisler  Multi-processor Interrupt Ctrl.    
            APB: 80000200 - 80000300
  gptimer0  Cobham Gaisler  Modular Timer Unit    
            APB: 80000300 - 80000400
            IRQ: 8
            16-bit scalar, 2 * 32-bit timers, divisor 50
  gpio0     Cobham Gaisler  General Purpose I/O port    
            APB: 80000900 - 80000A00
  spi0      Cobham Gaisler  SPI Controller    
            APB: 80000A00 - 80000B00
            IRQ: 10
            FIFO depth: 4, 1 slave select signals
            Maximum word length: 32 bits
            Supports 3-wire mode
            Controller index for use in GRMON: 0
  ahbstat0  Cobham Gaisler  AHB Status Register    
            APB: 80000F00 - 80001000
            IRQ: 1
  
grmon2> 
\end{verbatim}

De esta información se puede destacar:
\begin{itemize}
\item La frecuencia de reloj de sistema detecta es de $50\ \mbox{MHz}$, coincidente con el reloj disponible en el kit de desarrollo.
\item Existe una única instancia del procesador LEON3 (configuración monocore), el cual aparece enumerado como \textit{cpu0}. Este procesador cuenta con 8 ventanas de registros e implementa las instrucciones de multiplicación y división por hardware, como lo indican los items \textit{win 8} y \textit{V8 mul/div} bajo el apartado de la DSU del sistema (\textit{dsu0}).
\item El sistema cuenta con un único módulo APBUART, que se lista como \textit{uart0}. Éste se encuentra mapeada en el rango de direcciones de memoria que va de 0x80000100 a 0x80000200, tiene asignada la interrupción 2, e implementa FIFOs en los canales de transmisión y recepción.
\item Un único módulo GPTIMER, en el rango de direcciones de 0x80000300 a 0x80000400, IRQ 8. Este módulo contiene dos temporizadores de 32 bits y un divisor de entrada de 16 bits de ancho. El valor del divisor de este módulo tiene actualmente el valor 50 para alimentar los temporizadores con una señal de $1\ \mbox{MHz}$.
\item Un controlador de interrupciones IRQMP mapeado en el rango de direcciones de 0x80000200 a 0x80000300.
\end{itemize}

Si bien GRMON puede utilizarse para cargar y correr programas de forma independiente, y cuenta con un conjunto básico de comandos que permiten realizar la depuración de un programa en ejecución (inspección de estado de pila, ejecución paso a paso, inserción de puntos de parada, etc.), para trabajar con el Firmware CIAA es preferible la utilización del programa GDB para plataformas LEON3. 

Para utilizar GDB es necesario realizar la conexión entre GRMON y el sistema LEON3, y luego ejecutar el comando ``gdb'' en la consola interactiva de GRMON.
\begin{verbatim}
grmon2> gdb
  Started GDB service on port 2222.
  
grmon2> 
\end{verbatim}
Este comando abre un servidor del protocolo de depuración remota de GDB a través del puerto TCP 2222 de la máquina local, al que puede conectarse una sesión remota de GDB como cliente. En este estado GRMON actúa de pasarela entre el cliente GDB y la plataforma de hardware, permitiendo la carga y ejecución de programas, el análisis del contenido de memoria, la utilización de puntos de parada, y otras muchas funcionalidades de depuración. 

Una sesión de depuración del Firmware CIAA que utiliza GDB puede ser iniciada utilizando el comando ``make debug'' en el directorio del \textit{Makefile} del Firmware CIAA. Este comando asume que el usuario inició el programa GRMON en otra consola e inició el servidor de depuración remota de la forma indicada más arriba.

A continuación se resume una sesión de carga del Firmware CIAA en la plataforma dejando la ejecución detenida al comienzo de la rutina \textit{main()} del programa.
\begin{verbatim}
glpuga@HOST:~/CIAAFirmwareSource$ make clean
glpuga@HOST:~/CIAAFirmwareSource$ make all
glpuga@HOST:~/CIAAFirmwareSource$ make debug

==========================================================
Linking file: ./out/bin/example.exe
 
sparc-elf-gcc ./out/obj/example.o -Xlinker 
              --start-group  ./out/lib/rtos.a  
              ./out/lib/drivers.a  ./out/lib/posix.a
              ./out/lib/libs.a  ./out/lib/ciaak.a -Xlinker
              --end-group -o ./out/bin/example.exe
              -g -O0  -msoft-float -mcpu=v8 
 
==========================================================
Post Building example
 
==========================================================
Starting GDB...
 
sparc-elf-gdb ./out/bin/example.exe -ex "target 
              extended-remote 127.0.0.1:2222"

[... GDB LICENCE INFORMATION ... ]

A program is being debugged already.  Kill it? (y or n) y
Remote debugging using 127.0.0.1:2222
OSEK_TASK_InitTask () at examples/example/src/example.c:167
167	   }
(gdb) load
Loading section .text, size 0xaf40 lma 0x40000000
Loading section .data, size 0xa50 lma 0x4000af40
Start address 0x40000000, load size 47504
Transfer rate: 962106 bits/sec, 505 bytes/write.
(gdb) bre main
Breakpoint 1 at 0x40001aa8: file 
              examples/example/src/example.c, line 144.
(gdb) cont
Continuing.

Breakpoint 1, main () at examples/example/src/example.c:144
144	   StartOS(AppMode1);
(gdb) 
\end{verbatim}

  
\subsection{Modelo simulado en TSIM}

TSIM es una pieza de software desarrollada por Cobham Gaisler que sirve para simular sistemas basados en los procesadores espaciales ERC32, LEON2, LEON3 y LEON4 con un elevado nivel de precisión y fidelidad a las implementaciones por hardware. 

Algunas de las características más sobresalientes del TSIM son las siguientes:
\begin{itemize}
\item Emulación de procesadores ERC32, LEON2, LEON3, y LEON4 (estos últimos en configuraciones no multicore).
\item Elevada velocidad de simulación: hasta 60 MIPS en computadoras de altas prestaciones (Intel i7-2600K @$3.4\ \mbox{GHz}$)
\item Operación aislada o ligado al depurador GDB a través de un canal de depuración remota.
\item Permite modificar las características de los módulos GRLIB que forman parte del sistema simulado.
\item La versión comercial puede simular los sistemas durante intervalos arbitrariamente extensos de tiempo.
\item Admite la incorporación de módulos externos para incorporar dispositivos definidos por el usuario al sistema bajo simulación.
\item Cuenta con modelos funcionales predefinidos para varios de los sistemas LEON comercializados en ASIC:  AT697 (LEON2FT), UT699 (LEON3FT), UT700 (LEON3FT).
\end{itemize}

TSIM es un producto comercial de Cobham Gaisler por lo que no es un software de libre uso. Sin embargo existe una versión de evaluación del programa que puede ser descargada desde la página web de la empresa, la cual presenta una serie de limitaciones no presentes en la versión comercial: solamente puede simular sistemas basados en LEON3, el tiempo máximo de simulación está acotado, no es posible utilizar módulos de usuario para extender el sistema, no se puede modificar las características de la arquitectura de memoria, y algunas otras. 

Aun con estas limitaciones la versión de evaluación de TSIM es perfectamente capaz de generar un sistema LEON3 totalmente funcional que cumpla con los requerimientos mínimos necesarios para correr el Firmware CIAA, y además brinda una amplia flexibilidad a la hora de definir muchas de las características del hardware. Por estas razones se utilizó la versión de evaluación de TSIM para simular las plataformas de hardware utilizadas para correr muchos de los ensayos de validación del firmware.

Al igual que GRMON, TSIM también puede instanciar un servidor del protocolo de depuración remota a través del cual GDB puede controlar completamente el sistema simulado, cargar programas, establecer puntos de parada, examinar variables, etc. Este mecanismo permite reemplazar una plataforma física controlada a través de GRMON por una plataforma virtual simulada en TSIM de forma totalmente transparente para el Firmware CIAA y los procedimientos de depuración habituales. 

El sistema básico generado por TSIM consta de los siguientes módulos: 
\begin{itemize}
\item Procesador LEON3.
\item Controlador de memoria.\begin{itemize} 
\item 2MB de memoria ROM
\item 4 MB de memoria SRAM
\item 32 MB de memoria SDRAM
\end{itemize}
\item Controlador de interrupciones IRQMP.
\item Módulo GPTIMER.
\item Dos módulos APBUART.
\end{itemize}
La información de estos dispositivos se encuentra disponible mediante el sistema de enumeración de la biblioteca GRLIB, en las tablas de dispositivos AHB y APB correspondientes.

Los dispositivos que forman parte del sistema anterior pueden ser configurados en gran medida mediante una serie de argumentos pasados a través de la línea de comandos a TSIM. Un resumen de los argumentos más relevantes se puede encontrar en la Tabla \ref{tab.argumentostsim}; una lista completa de los argumentos es muy extensa para incluirse en este documento, pero se puede ser consultar una en la referencia \cite{ref.tsimusersmanual}.

\begin{table}[tbp]
	\centering
	\caption[Argumentos relevantes del programa TSIM.]{Resumen de los argumentos más relevantes del programa TSIM que se usan para establecer las características del sistema simulado. Una lista completa de argumentos puede encontrarse en la referencia \cite{ref.tsimusersmanual}.}
	\begin{tabular}{l | p{93mm}}    
		\toprule
		\textbf{Argumento}  & \textbf{Detalle} \\
		\midrule
-freq \textit{<frecuencia>} & Configura la frecuencia del reloj del sistema. \\
-nfp                        & Deshabilita la unidad de punto flotante. \\
-uart\_fs \textit{<N>}      & Configura los buffers de transmisión y recepción de las UARTS. $N$ puede tomar loa valores 1, 2, 4, 8, 16 y 32. Si $N > 1$ las UARTs simuladas contarán con FIFOs e incorporarán los bits de estado y control correspondientes; en caso contrario las UARTs solamente cuentan con un registro de transmisión/recepción simple y con el conjunto de bits de estado y control es el estándar. \\
-nrtimers \textit{<N>}      & Cantidad de temporizadores en el módulo GPTIMER. El valor de $N$ debe ser un número entre 2 y 8. \\
-sametimerirq & Si se encuentra presente todos los temporizadores utilizan la misma IRQ para generar interrupciones. \\
-nwin \textit{<N>}          & Configura la cantidad de ventanas de registros implementadas en el procesador. \\
-hwbp                       & Usar \textit{breakpoints} por hardware para todos los \textit{breakpoints} configurados por GDB. \\
-numbp \textit{<N>}         & Máxima cantidad de \textit{breakpoints} por hardware que puede configurar el sistema. \\
-numwp \textit{<N>}         & Máxima cantidad de \textit{watchpoints} por hardware que puede configurar el sistema. \\
-uart1 <dispositivo>        & Hace accesibles las transmisiones y recepciones del primer módulo APBUART a través de la pseudoconsola \textit{dispositivo}, que puede ser accedida mediante los programas GTKTerm, Minicom, o cualquier otro programa semejante. Si no se especifica el primer módulo APBUART queda conectado a la entrada y salida estándar de TSIM. \\
-uart2 <dispositivo>        & Hace accesibles las transmisiones y recepciones del segundo módulo APBUART a través de la pseudoconsola \textit{dispositivo}, que puede ser accedida mediante los programas GTKTerm, Minicom, o cualquier otro programa semejante. \\ 
-gdb                        & Al recibir este argumento TSIM no ofrece una consola interactiva sino que abre un servidor de protocolo GDB de depuración remota. \\
-port \textit{<port>}       & Configura el número de puerto utilizado para el servidor de protocolo GDB de depuración remota. Si no se especifica otra cosa, el puerto por defecto es el 1234; para utilizar el mismo puerto que utiliza el GRMON debe configurarse el puerto 2222. \\
		\bottomrule
	\end{tabular}
	\label{tab.argumentostsim}
\end{table}

Una invocación de TSIM como la siguiente crea un sistema simulado capaz de correr el Firmware CIAA y que puede ser controlado mediante GDB a través del protocolo de depuración remota.
\begin{verbatim}
glpuga@HOST:~$ tsim-leon3 -freq 40 -uart_fs 8 -nfp ...
                          -nrtimers 4 -hwbp -numbp 64 ...
                          -numwp 64 -sametimerirq ...
                          -port 2222 -gdb 

 TSIM/LEON3 SPARC simulator, version 2.0.47 (eval. version)

 Copyright (C) 2016, Cobham Gaisler - all rights reserved.
 This software may only be used with a valid license.
 For latest updates, go to http://www.gaisler.com/
 Comments or bug-reports to support@gaisler.com

FPU disabled
system frequency: 40.000 MHz
serial port A on stdin/stdout
allocated 4096 KiB SRAM memory, in 1 bank
allocated 32 MiB SDRAM memory, in 1 bank
allocated 2048 KiB ROM memory
icache: 1 * 4 KiB, 16 bytes/line (4 KiB total)
dcache: 1 * 4 KiB, 16 bytes/line (4 KiB total)
gdb interface: using port 2222
\end{verbatim}
Una vez puesto en marcha el programa TSIM de la forma indicada se puede iniciar la ejecución del Firmware CIAA mediante una secuencia idéntica a la utilizada al final de la Sección \ref{sec.modelohardware}.

\section{Validación módulo rtos}

Tal como se mencionó anteriormente, el Firmware CIAA cuenta con un sistema automatizado de tests que permite validar el sistema operativo OSEK-OS mediante la ejecución de una batería de ensayos estandarizados. 

Esta batería de ensayos fue ejecutada sobre los dos modelos de sistema disponibles para evaluar el funcionamiento del código desarrollado para habilitar la ejecución del sistema operativo OSEK-OS del Firmware CIAA sobre sistemas basados en el procesador LEON3. 

En la plataforma DE2-115 se ejecutó la batería de ensayos una única vez, ya que esta no cuenta con parámetros fácilmente modificables que pudieran utilizarse para introducir alternativas en la ejecución. 

En la plataforma simulada utilizando TSIM, en cambio, se evaluaron los tests de la batería de ensayos múltiples veces, modificando el parámetro de frecuencia del reloj del sistema, para introducir variabilidad en la ejecución de los ensayos.

\begin{figure}[p]
	\centering
	\includegraphics[width=0.99\textwidth]{./Figures/resultadovalidacionrtos.png}
	\caption[Resultado de una corrida de la batería de ensayos de validación de OSEK-OS sobre LEON3.]{Captura de pantalla donde se ve el resultado de correr la batería de ensayos estandarizados sobre el Firmware CIAA para procesador LEON3.}
	\label{fig.resultadoensayovalidacion}
\end{figure}

El resultado final de una de las corridas de los ensayos puede verse en la captura de ventana de la Figura \ref{fig.resultadoensayovalidacion}, donde se pueden ver los últimos renglones de la salida estándar de la ejecución de ``make rtostests'' y el resumen final de los resultados obtenidos, que se repite a continuación:
\begin{verbatim}
************** RTOS Test Summary Results **************
Total Tests: 244   Tests OK: 244   Tests FAILED: 0
\end{verbatim}

El sumario de resultados para todos los casos se puede ver en la Tabla \ref{tab.resultadovalidacionrtos}.

\begin{table}[p]
	\centering
	\caption[Resultados de la validación del módulo \textit{rtos}.]{Sumario de resultados de los ensayos de validación del módulo \textit{rtos} para cada una de las plataformas y variantes de la misma.}
	\begin{tabular}{c | c | c | p{60mm}}    
		\toprule
		\textbf{Plataforma} & \textbf{Frecuencia} & \textbf{Resultado} & \textbf{Comentarios} \\
		\midrule
		DE2-115             & $50\ \mbox{MHz}$             & satisfactorio    & Ninguno.  \\
		\hline
%        TSIM                & $20\ \mbox{MHz}$             & problemático     & Presenta errores durante los tests ctest\_al\_02 y ctest\_al\_03. Probablemente sea necesario incrementar la pila de las tareas. \\
%        TSIM                & $30\ \mbox{MHz}$             & problemático     & Presenta errores durante los tests ctest\_al\_02 y ctest\_al\_03. Probablemente sea necesario incrementar la pila de las tareas. \\
        TSIM                & $40\ \mbox{MHz}$             & satisfactorio    & Ninguno.  \\
        TSIM                & $50\ \mbox{MHz}$             & satisfactorio    & Ninguno.  \\
        TSIM                & $75\ \mbox{MHz}$             & satisfactorio    & Ninguno.  \\
        TSIM                & $100\ \mbox{MHz}$            & satisfactorio    & Ninguno.  \\
        TSIM                & $150\ \mbox{MHz}$            & satisfactorio    & Ninguno.  \\
        TSIM                & $200\ \mbox{MHz}$            & satisfactorio    & Ninguno.  \\
		\bottomrule
	\end{tabular}
	\label{tab.resultadovalidacionrtos}
\end{table}

Para lograr la ejecución exitosa de la batería de ensayos fue necesario incrementar los tamaño de las pilas de las tareas que los componen, modificando manualmente los archivos OIL de las plantillas de las que se derivan los programas de prueba.

Esto fue necesario porque los tamaños de las pilas que estaban configurados previamente, aunque eran perfectamente válidos para otras plataformas (Cortex-M, x86), eran insuficientes para ejecutar esos mismos programas en un arquitectura SPARC como el procesador LEON3. 

La razón de esto es que como se mencionó en el Capítulo \ref{Chapter2} el mecanismo de ventanas de registros utilizado en SPARC requiere un mínimo de 96 bytes por cada nivel de anidamiento de subrutinas o interrupciones se apilen en el \textit{stack}. Se puede contrastar esto contra el tamaño del espacio de pila originalmente configurado para algunas de las tareas de la batería de ensayos, que era de unos magros 128 bytes.

Debe notarse que si bien la utilización de un sistema real (por ejemplo, el modelo implementado en el kit DE2-115) a través de GRMON para correr los ensayos no presenta problemas conocidos, la utilización de un sistema simulado en TSIM presenta un par de características que deben ser tenidas en cuenta.

La primera es que se observó durante la ejecución de algunas corridas de la batería de ensayos que el sistema de validación se detenía, arrojando un error de conexión. A partir de una análisis de las circunstancias y de los errores generados por las partes, se concluyó que esto no parece tener relación alguna con problemas en el código del Firmware CIAA que estaba bajo prueba, sino con una condición de carrera que resulta de la interacción entre el sistema de test y el programa TSIM.

El error se debe a que entre el final de un ensayo y el comienzo del ensayo siguiente TSIM detecta la desconexión del cliente GDB, cierra el puerto TCP del servidor del protocolo remoto e inmediatamente vuelve a abrirlo. Sin embargo, en algunas ocasiones la reapertura del puerto se demora una fracción de segundo y el servidor no está listo cuando durante el ensayo siguiente se intenta conectar una nueva sesión del programa GDB a TSIM. Esto resulta en un error fatal de conexión que se propaga y paraliza completamente el sistema de test. 

En todos los casos donde se manifestó este problema la situación se salvó simplemente volviendo a correr los ensayos desde el principio, ya que la aparición de la condición de carrera es infrecuente y puramente fortuita.

Otro con TSIM se observa si se ejecuta la batería de ensayos inmediatamente a continuación de iniciar el programa TSIM; en este caso el primer ensayo del conjunto  sistemáticamente arroja una falla. En este caso el problema se debe a que TSIM parece no procesar correctamente el comando ``reset'' utilizado para reiniciar el hardware simulado entre ensayos, cuando este se ejecuta inmediatamente luego de iniciado el simulador. 

Dado que no es posible prescindir de la utilización del comando ``reset'' para refrescar la plataforma entre ensayos, en este caso el problema se debe salvar simplemente haciendo una corrida preparatoria: se inicia TSIM, se inicia una corrida preparatoria de la batería de tests, se la interrumpe luego de realizados un par de ensayos, y sin cerrar TSIM se vuelve correr la batería de ensayos, esta vez de forma definitiva. Esta nueva corrida debería ejecutar todos los ensayos satisfactoriamente.

\subsection{Medición de intervalos de tiempo}

Si bien la medición de intervalos de tiempo es una de la funciones del sistema operativo OSEK-OS, la batería de ensayos estandarizados ejecutada anteriormente sólo verifica que las funciones involucradas en esta función devuelvan los códigos de error correspondientes cuando los argumentos de las mismas son inválidos, pero no realiza ninguna comprobación acerca de la duración real de los intervalos generados por el sistema. La razón de esto es que la duración del \textit{tick} del sistema no se encuentra definida por el estándar OSEK-OS. 

Tal como se explicó en el Capítulo \ref{Chapter3}, en el Firmware CIAA para LEON3 las duraciones de los \textit{ticks} se encuentran definidas por la Tabla \ref{tab.timersdefinidos}. La correcta generación de estos intervalos de referencia debió ser validada mediante una serie de ensayos dedicados.

Con este fin se desarrolló un programa simple que transmitía una cadena de texto a través de la UART a intervalos de tiempo bien definidos. Este programa fue puesto en marcha, y externamente se cronometró el intervalo entre recepciones de la cadena de texto para medir el intervalo real generado por el sistema y contrastarlo con el valor programado en el código.

Los resultados fueron satisfactorios, verificando así el correcto funcionamiento de los algoritmos de configuración del módulo GPTIMER y de la rutina de servicio de interrupción asociada.


\section{Validación módulo drivers}

La validación del funcionamiento del driver UART se realizó de forma más informal que lo visto anteriormente para el módulo \textit{rtos}. 

En este caso no existen ensayos predefinidos que permitan garantizar el funcionamiento bajo todos los casos posibles, y dado que la temporización relativa de los eventos es muy relevante en la evolución del sistema ni siquiera está claro que exista una cantidad de casos finitos que sea posible comprobar sistemáticamente. 

Por estas razones la validación del funcionamiento del driver se llevó a cabo mediante la creación de pequeños programas dedicados que  verifican el funcionamiento de la transmisión y recepción a través de la UART, de la interfaz con la capa POSIX, de los buffers de bajo nivel, y de las rutinas de servicio de interrupciones asociadas.

Se verificó así la capacidad de transmitir conjuntos de datos de diferentes largos, ejercitando la gestión de los buffers intermedios en todos los niveles (capa POSIX, capa de gestión de interrupciones del driver, y capa de gestión del buffer FIFO de la UART) y la capacidad de recibir bytes y encolarlos hasta que la aplicación los demande mediante una operación \textit{read()} a la capa POSIX. 

Aprovechando la flexibilidad del TSIM para redefinir las características del hardware, todos los ensayos se repitieron múltiples veces haciendo variar en cada caso el tamaño del FIFO de transmisión/recepción de las UARTs.

Queda pendiente la puesta en marcha y verificación del driver cuando el módulo APBUART no implementa FIFOs. La puesta en marcha de este caso se vio dificultada por lo que parecen ser algunas idealizaciones en el modelo simulado por TSIM, el cual transfiere los datos escritos en el registro de salida al registro de desplazamiento de la UART con velocidad infinita.

Con la salvedad de lo comentado en párrafo anterior, los resultados de todos los ensayos realizados sobre este módulo fueron satisfactorios. 

%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
